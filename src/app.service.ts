import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  sayHello() {
    return {
      node_env: process.env.NODE_ENV,
      port: process.env.PORT,
      message: 'Hello Docker from Nest!',
      time: Date.now().toString(),
    };
  }
}
