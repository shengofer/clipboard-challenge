import { Injectable } from '@nestjs/common';
import { EMPLOYEE_DB } from '../employee/employee.dataset';


@Injectable()
export class SalarySummaryService {
  private employeeList = EMPLOYEE_DB;

  getSummary() {
    return Promise.resolve(this.getStatistics(this.employeeList));
  }

  getContractorSummary() {
    let contractors = this.employeeList.filter((elem) => !!elem.on_contract)
    console.log(contractors);
    return Promise.resolve(this.getStatistics(contractors));
  }

  getSummaryByDepartment() {
    return Promise.resolve(this.getStatistics(this.employeeList));
  }

  private getStatistics(employeeList) {
    let min = employeeList[0].salary;
    let max = employeeList[0].salary;
    let sum = 0;

    for (let i = 0; i < employeeList.length; i++) {
      const empl = employeeList[i];
      let salary = Number.parseInt(empl.salary);
      min = salary> min? min: salary;
      max = salary > max? salary: max;
      sum = sum + salary
    }
    let mean = sum/employeeList.length
    return {
      min,
      mean,
      max,
    }
  }

}
