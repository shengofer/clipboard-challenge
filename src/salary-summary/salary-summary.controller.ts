import { Body, Controller, Get } from '@nestjs/common';
import {  SalarySummaryService } from './salary-summary.service';


@Controller('salary')
export class SalarySummaryController {
  constructor(public salarySummaryService: SalarySummaryService) {
  }

  @Get('')
  async getSummary(): Promise<any> {
    return await this.salarySummaryService.getSummary();
  }

  @Get('/contractors')
  async getContractorSummary(): Promise<any> {
    return await this.salarySummaryService.getContractorSummary();
  }

}

