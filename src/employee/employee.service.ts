import { Injectable } from '@nestjs/common';
import { EMPLOYEE_DB } from './employee.dataset';
import { Employee } from './employee.model';

@Injectable()
export class EmployeeService {
  private employeeList = EMPLOYEE_DB;

  get() {
    return Promise.resolve(this.employeeList);
  }

  create(employee: Employee) {
    this.employeeList.push(employee);
    return Promise.resolve(employee);
  }

  deleteByName(name) {
    this.employeeList = EMPLOYEE_DB.filter((obj) => {
      return obj.name !== name;
    });
  }
}
