export class Employee {
    name:string;
    salary: string;
    currency: string;
    department: string;
    sub_department: string;
    on_contract: string;
}