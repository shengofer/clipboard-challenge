import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { Employee } from './employee.model';
import { EmployeeService } from './employee.service';


@Controller('employee')
export class EmployeeController {
  constructor(public employeeService: EmployeeService) {
  }

  @Get('')
  async get(): Promise<any[]> {
    return await this.employeeService.get();
  }
  @Post('')
  async create(@Body() employee: Employee) {
    console.log('EKMP', employee);
   return  await this.employeeService.create(employee);
  }

  @Delete('/:name')
  async delete(@Param('name') name) {
    await this.employeeService.deleteByName(name);
  }
}

