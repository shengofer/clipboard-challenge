import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { EmployeeController } from './employee/employee.controller'
import { SalarySummaryController } from './salary-summary/salary-summary.controller';

import { AppService } from './app.service';
import { EmployeeService } from './employee/employee.service';
import { SalarySummaryService } from './salary-summary/salary-summary.service';

@Module({
  imports: [],
  controllers: [AppController, EmployeeController, SalarySummaryController],
  providers: [AppService, EmployeeService, SalarySummaryService],
})
export class AppModule {}
